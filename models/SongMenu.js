const mongoose = require("mongoose");
const db = require('./db.js');
const getDate = require("./date.js");
const token = require("./token.js");
const Schema = mongoose.Schema;

const SongMenuShema = new Schema({
  menuname: String,
  intro: String,
  songs: Array, //这里存的很多首歌，每一首歌是一个对象有songId 和 songType两个属性  
  creator: String //歌单的创建者，值为创建该歌单的用户的id
})
let SongMenuModel = db.model('SongMenu', SongMenuShema);
//menuname, intro, songs, creator 构成menuObj
class SongMenu {
  constructor(menuObj) {
    for (let key in menuObj) {
      this[key] = menuObj[key];
    }
  }
  create() {
    
    const mEntity = new SongMenuModel({
      menuname: this.menuname,
      intro: this.intro,
      songs: this.songs,
      creator: this.creator
    });
    return mEntity.save();
  }
}
//接口处需要token
SongMenu.getByCreator = (creator) => {
  return SongMenuModel.find({
    creator: creator
  }).exec();
}
SongMenu.getByMid = (mid) => {
  return SongMenuModel.findOne({
    _id: mid
  }).exec();
};

SongMenu.remove = (mid) => {
  return SongMenuModel.findByIdAndRemove(mid).exec();
}
module.exports = SongMenu;