var crypto = require('crypto');

function generate(secret,time){
  var hash = crypto.createHmac('sha256', secret)
                    .update(time)
                    .digest('hex');
  return hash;
}
module.exports = generate;