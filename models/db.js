var mongoose = require('mongoose');    //引用mongoose模块
mongoose.Promise = global.Promise;

var db = mongoose.createConnection('mongodb://localhost:13002/musicdb'); //创建一个数据库连接

db.on('error',console.error.bind(console,'连接错误:'));
db.once('open',function(){
  console.log("mongodb connection success");
});

module.exports = db;