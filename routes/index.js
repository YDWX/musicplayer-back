const express = require('express');
const router = express.Router();
const singsdk = require("5sing-sdk");
const User = require("../models/User");
const SongMenu = require("../models/SongMenu");
router.get('/', (req, res, next) => {
  res.render('index', { title: 'Express' });
});
//关键字搜索
router.get('/search/', (req, res, next) => {
  if(!req.query.keyword||!req.query.pageIndex){
    res.status(400).json({success:false,message:"missing param",data:[]});
    return;
  }
  singsdk.search({
    keyword:req.query.keyword,
    pageIndex:req.query.pageIndex
  },(result,response) => {
    if(result.success){
      res.json({success:true,status:200,message:"search song success",data:result.data.songArray});
    }else{
      res.json({success:false,status:400,message:"keyword error or no such kind",data:[]});
    }
  },(err,result,response) => {
    res.status(500).json({success:false,message:"server error",data:[]});
  })
});
//获取歌曲详情
router.get('/getSong/', (req, res, next) => {
  var query = req.query;
  if(!query.songId || !query.songType){
    res.status(400).json({success:false,message:"missing param"});
    return;
  }
  singsdk.getSong({
    songId:query.songId,
    songType:query.songType
  },(result,response) => {
    if(result.success){
      res.json({success:true,status:200,message:"song detail success",data:result.data});
    }else{
      res.json({success:false,status:400,message:"param error or no such song",data:[]});
    }
  },(err,result,response) => {
    res.status(500).json({success:false,message:"server error",data:[]});
  })
});

//注册
router.post('/signup/',(req, res, next) => {
  let body = req.body;
  console.log(req.body);
  if(!body.uid||!body.password||!body.nickname){
    res.status(400).json({success:false,status:400,message:"missing param",token:""});
    return;
  }
  let newuser = new User(body.uid,body.nickname,body.password);
  newuser.signup().then((doc) => {
    if(doc === 410){
      res.json({success:false,status:410,message:"user already exist",token:""});
    }
    else if(doc){
      res.json({success:true,status:200,message:"signup success",token:doc.token});
    }
  })
});

//登录
router.post('/login/',(req, res, next) => {
  let body = req.body;
  if(!body.uid||!body.password){
    res.status(400).json({success:false,status:400,message:"missing param", token:""});
    return;
  }
  var promise = User.login(body.uid,body.password);
  promise.then((doc) => {
    if(doc === 408){
      res.json({success:false,status:408,message:"invalid password", token:""});
    }else if(doc === 409){
      res.json({success:false,status:409,message:"register first", token:""});
    }else if(doc === 500){
      res.status(500).json({success:false,status:500,message:"server error", token:""});
    }else{
      res.json({success:true,status:200,message:"login success",token:doc.token});
    }
  })
});

router.get('/getnickname/', (req, res, next) => {
  const query = req.query;
  if(!query.token){
    res.status(401).json({success:false,status:401,message:"no authorized"});
    return;
  }
  User.checkToken(query.token).then((doc) => {
    if(doc === 1){
      res.status(500).json({success:false,status:500,message:"server error"});
    }else if(doc){
      res.json({success:true,status:200,message:"get nickname success",data:{nickname:doc.nickname}});
    }else{
      res.json({success:false,status:405,message:"token has expired"});
    }
  })
})
//添加歌单
router.post('/addMenu/',(req, res, next) => {
  const body = req.body;
  if(!body.token){
    res.status(401).json({success:false,status:401,message:"no authorized"});
    return;
  }else if(!body.menuObj){
    res.status(400).json({success:false,status:400,message:"missing param"});
    return;
  }
  User.checkToken(body.token).then((doc) => {
    if(doc === 1){
      res.status(500).json({success:false,status:500,message:"server error"});
    }else if(doc){
      const menu = new SongMenu(JSON.parse(body.menuObj));
      menu.create().then((menu)=>{
        res.json({success:true,status:200,message:"menu created",data:{mid:menu._id}});
      });
    }else{
      res.json({success:false,status:405,message:"token has expired"});
    }
  })
})

//获取歌单
router.post('/getMenu/', (req, res, next)=>{
  const body = req.body;
  if(!body.token){
    res.status(401).json({success:false,status:401,message:"no authorized",menu:""})
  }
  User.checkToken(body.token).then((doc) => {
    if(doc === 1){
      res.status(500).json({success:false,status:500,message:"server error",menu:""});
    }else if(doc === 2){
      res.json({success:false,status:400,message:"invalid token",menu:""});
    }else if(doc){
      SongMenu.getByCreator(doc.uid).then((menu)=>{
        res.json({success:true,status:200,message:"get menu success", menu:menu});
      });
    }else{
      res.json({success:false,status:200,message:"token has expired",menu:""});
    }
  })
})

//通过歌单id获取歌单，用于分享歌单（不需要token）
router.get('/getMenu/:id', (req, res, next) => {
  const menuid = req.params.id;
  SongMenu.getByMid(menuid).then((menu) => {
    if(!menu){
      res.json({success:false, status:409, message:"menu not exist",menu:{}});
    }else{
      res.json({success:true, status:200, message:"get menu success", menu:menu});
    }
  },(err)=>{

    res.json({success:false,status:200,message:"invalid id",menu:{}});
  })
});

router.post('/removeMenu/', (req, res, next) => {
  const token = req.body.token;
  const mid = req.body.mid;
  User.checkToken(token).then((doc) => {
    if(doc === 1){
      res.status(500).json({success:false,status:500,message:"server error"});
    }else if(doc === 2){
      res.json({success:false,status:400,message:"invalid token"});
    }else if(doc){
      SongMenu.remove(mid).then(() => {
        res.json({success:true,status:200,message:"remove menu success"});
      })
    }else{
      res.json({success:false,status:200,message:"token has expired"});
    }
  })
})
module.exports = router;